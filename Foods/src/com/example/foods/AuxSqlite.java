package com.example.foods;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class AuxSqlite extends SQLiteOpenHelper 
{
	private String  msg= "";
	public String getMsg(){ return msg;}
	
	//Sentencia SQL para crear la tabla de Usuarios
	String sqlCreate = "CREATE TABLE Comida (nombre TEXT, pais TEXT)";
	
	private String nombd;
	
	public String getNombd()
	{ return this.nombd; }
	
	public AuxSqlite(Context contexto, String nombre, CursorFactory factory, int version) //string nombre de la bd,curso factory null,int version
	{
	    super(contexto, nombre, factory, version);
	    this.nombd = nombre;
	}
	 
	@Override
	public void onCreate(SQLiteDatabase db) // solo se ejecuta cuando no existe la BD,s
	{
		//Se ejecuta la sentencia SQL de creaci??n de la tabla
	  
	    try{
			db.execSQL(sqlCreate);
			
			msg ="funciona";
		}
		catch(Exception e)
		{
			msg = e.getMessage();
		}
	    
	}
	
	
	
	
	
	 
	@Override
	public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) //metodo se ejecuta cuando se actualiza la app!!!
	{
		//NOTA: Por simplicidad del ejemplo aqu?? utilizamos directamente la opci??n de
	    //      eliminar la tabla anterior y crearla de nuevo vac??a con el nuevo formato.
	    //      Sin embargo lo normal ser?? que haya que migrar datos de la tabla antigua
	    //      a la nueva, por lo que este m??todo deber??a ser m??s elaborado.
	 
	    //Se elimina la versi??n anterior de la tabla
	    db.execSQL("DROP TABLE IF EXISTS Usuarios");
	 
	    //Se crea la nueva versi??n de la tabla
	    db.execSQL(sqlCreate);
	}
	
	public boolean insert(SQLiteDatabase db, String tabla, String[] campos, String[] valores)//insertar que cualquier tipo de tabla
	{
		String query = "INSERT INTO @tabla (@campos) VALUES (@valores)";
		
		String fields = "";
		for (int i = 0; i < campos.length; i++)
			fields+=campos[i] + ",";
		fields = fields.substring(0, fields.length() - 1);
		
		String values = "";
		for (int i = 0; i < valores.length; i++)
			values += ("'" + valores[i] + "',");
		values = values.substring(0, values.length() - 1);
		
		query = query.replace("@campos", fields).replace("@valores", values).replace("@tabla", tabla);
		
		try
		{
			db.execSQL(query);
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}
	
}