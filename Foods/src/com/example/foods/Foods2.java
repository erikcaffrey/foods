package com.example.foods;


import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Foods2 extends Activity {
    Button otro;
    EditText nombre;
    EditText pais;
    
  
    
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.foods2);
		
		final AuxSqlite cnx = new AuxSqlite(this,"BDComidas.db",null,1);
		
		
		// API 10 en adelante
		SQLiteDatabase db = cnx.getWritableDatabase();
		Toast.makeText(this, cnx.getMsg(), 2).show();
       		
		
		otro = (Button) findViewById(R.id.button3);
		 nombre = (EditText) findViewById(R.id.editText1);
		pais = (EditText) findViewById(R.id.editText2);
		
		
		otro.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String nombresito = nombre.getText().toString();
				String paisito = pais.getText().toString();
				
				if(nombresito.equals("") || paisito.equals("")){
					
					Toast.makeText(Foods2.this, "Error Cajas vacias", 2).show();
					
				}
				else{
					
					Toast.makeText(Foods2.this, "Correcto", 2).show();
					
					SQLiteDatabase bd = cnx.getWritableDatabase();
					if(cnx.insert(bd,"Comida",new String[]{"nombre,pais"}, new String[]{
							nombresito,paisito })){
						Toast.makeText(Foods2.this, "Agregado correctamente", 2).show();
					}
					else{
						
						Toast.makeText(Foods2.this, "error al agregar correctamente", 2).show();
					}
					
					
					
					
					
					}
					
				}
				
				
			
		});
	}
		

}





	