package com.example.foods;
import java.util.ArrayList;
import java.util.List;



import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class Foods3 extends Activity {

	Spinner mostrar,mostrardos;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.foods3);
		
		final AuxSqlite cnx = new AuxSqlite(this,"BDComidas.db",null,1);
		SQLiteDatabase db = cnx.getReadableDatabase();
		
		mostrar = (Spinner) this.findViewById(R.id.spinner1);
		mostrardos = (Spinner) this.findViewById(R.id.spinner2);
		
		 List<String> items = new ArrayList<String>();
		
				 ArrayAdapter<String> adp = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, items);
				 //El Spinner ya debio ser referenciado
				 mostrar.setAdapter(adp);
				 
				 List<String> itemsdos = new ArrayList<String>();
					
				 ArrayAdapter<String> adap = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, itemsdos);
				 //El Spinner ya debio ser referenciado
				 mostrardos.setAdapter(adap);
		
		
		
	   
		//Si abro la base de datos
		
		if(db != null) {
		String query = "SELECT * FROM Comida "; 
				
		Cursor c = db.rawQuery(query, null);
		
		//Si hay registros
		if (c.moveToFirst()) {
		//Recorremos el cursor hasta que no haya más registros
		do {
		     //Devuelve el campo i del registro actual
		items.add( c.getString(0)) ;
		    
	    itemsdos.add(c.getString(1));
			
		    
	     	
		   } while(c.moveToNext());
		  
		 }
	}
			
		
	
  

  }
}


