package com.example.foods;




import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
AuxSqlite cnx = new AuxSqlite(this,"mibase.db",null,1);
		
		
		// API 10 en adelante
		
		SQLiteDatabase db = cnx.getWritableDatabase();
		Toast.makeText(this, cnx.getMsg(), 2).show();

		
		Button agregar = (Button) findViewById(R.id.button1);
		Button consultar = (Button) findViewById(R.id.button2);
		
		agregar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				Intent intentos = new Intent(MainActivity.this,Foods2.class);
				startActivity(intentos);
				
				
			}
		});
		
		consultar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			  	
				Intent intentos = new Intent(MainActivity.this,Foods3.class);
				startActivity(intentos);
				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	

}
